/* Requires */

const midi = require('midi');
const events = require('events');
const fs = require('fs');
const path = require('path');
const toml = require('toml');
const concat = require('concat-stream');
const cv = require('opencv4nodejs');
const cors = require('cors');

/* VJ Engine */

class VJEngine {

    /* Public attr */
    vcaps = new Array(8);
    input = new midi.Input();
    weights = [0, 0, 0, 0, 0, 0, 0, 0]
    speeds = 0; 
    track = 0;
    presetsSize = 0;
    presets = [];
    canny = false;
    currentPreset = {};
    eventEmitter = new events.EventEmitter();
    WSListeners = new events.EventEmitter();

    /* Private attr */
    #tmpCanny = false;
    #speedCanny = 100;
    #fixeCanny = false;

    constructor(presetsDir, h, w) {
        this.presetsDir = presetsDir;
        this.h = h;
        this.w = w;
        this.showDate = false;
    }

    /* MIDI Controls */

    async nextPreset() {
        if (this.track == this.presetsSize - 1)
            this.track = 0;
        else
            this.track++;
        await this.releasePresets();
        await this.reloadPreset();
    }

    async prevPreset() {
        if (this.track == 0)
            this.track = this.presets.length - 1;
        else
            this.track--;
        await this.releasePresets();
        await this.reloadPreset();
    }

    async startMIDI() {

        this.input.on('message', (deltaTime, message) => {
            if (message[1] === 0)
                this.weights[0] = message[2] * 0.008;
            else if (message[1] === 1)
                this.weights[1] = message[2] * 0.008;
            else if (message[1] === 2)
                this.weights[2] = message[2] * 0.008;
            else if (message[1] === 3)
                this.weights[3] = message[2] * 0.008;
            else if (message[1] === 4)
                this.weights[4] = message[2] * 0.008;
            else if (message[1] === 5)
                this.weights[5] = message[2] * 0.008;
            else if (message[1] === 6)
                this.weights[6] = message[2] * 0.008;
            else if (message[1] === 7)
                this.weights[7] = message[2] * 0.008;
            else if (message[1] === 58 && message[2] === 127)
                this.prevPreset();
            else if (message[1] === 59 && message[2] === 127)
                this.nextPreset();
            else if (message[1] === 46 && message[2] == 127) {
                if (this.canny === true) this.canny = false;
                else this.canny = true;
            } else if (message[1] === 42 && message[2] == 127) {
                if (this.#fixeCanny === true) this.#fixeCanny = false;
                else this.#fixeCanny = true;
            } else if (message[1] === 16) {
                if (message[2] === 127)
                    this.speeds = 0;
                else if (message[2] >= 2)
                    this.speeds += 0.15;
                else if (message[2] === 66 && this.speeds > 0)
                    this.speeds -= 1;
            } else if (message[0] === 176 && message[1] === 60 && message[2] === 0) {
                this.showDate = !this.showDate;
            }
        });

        /* Open MIDI port */
        try {
            this.input.openPort(1);
        } catch (err) {
            console.error("Impossible d'utiliser OpenVJ car le controleur MIDI n'est pas branché.");
            process.exit(1);
        }

    }

    /* TOML Configuration */

    getTOMLPreset(presetDir) {
        return new Promise((resolve, reject) => {
            fs.createReadStream(presetDir, 'utf-8').pipe(concat((data) => {
                try {
                    var presetTOML = toml.parse(data);
                } catch (err) {
                    console.error(err);
                    if (err) throw err;
                }

                resolve(presetTOML);
            }));
        });
    };

    /* Presets */

    getPresets() {
        return new Promise((resolve, reject) => {
            fs.readdir(this.presetsDir, { encoding: 'utf-8', withFileType: true }, (err, files) => {
                if (err) throw err;

                this.presetsSize = files.length;
                resolve(files);
            });
        });
    };

    reloadRead(vcap, w, h, t) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let frame = vcap.read();

                if (frame.empty) {
                    vcap.reset();
                    let frame = vcap.read().resize(w, h);

                    if (w !== undefined
                        && h !== undefined) resolve(frame);
                    else resolve(frame);
                } else {
                    frame = frame.resize(w, h);
                    if (w !== undefined
                        && h !== undefined) resolve(frame);
                    else resolve(frame);
                }
            }, t);
        });
    };

    async releasePresets() {
        return new Promise((resolve, reject) => {
            try {
                this.vcaps[0].release() && delete this.vcaps[0];
                this.vcaps[1].release() && delete this.vcaps[1];
                this.vcaps[2].release() && delete this.vcaps[2];
                this.vcaps[3].release() && delete this.vcaps[3];
                this.vcaps[4].release() && delete this.vcaps[4];
                this.vcaps[5].release() && delete this.vcaps[5];
                this.vcaps[6].release() && delete this.vcaps[6];
                this.vcaps[7].release() && delete this.vcaps[7];
                resolve();
            } catch (err) {
                console.error(err);
                if (err) throw err;
            }
        });
    }

    async reloadPreset() {
        let presetDir = path.resolve(
            path.join(this.presetsDir, this.presets[this.track], 'config.toml'));
        var presetConfig = await this.getTOMLPreset(presetDir);
        this.currentPreset = {
            title: presetConfig.header.name,
            description: presetConfig.header.description,
            autor: presetConfig.header.author,
            cover: presetConfig.header.cover
        };

        try {
            this.WSListeners.emit('preset-change');
            this.vcaps[0] = new cv.VideoCapture(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals.master.media));
            this.vcaps[1] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['1'].media)));
            this.vcaps[2] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['2'].media)));
            this.vcaps[3] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['3'].media)));
            this.vcaps[4] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['4'].media)));
            this.vcaps[5] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['5'].media)));
            this.vcaps[6] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['6'].media)));
            this.vcaps[7] = new cv.VideoCapture(path.resolve(path.join(this.presetsDir, this.presets[this.track], presetConfig.canals['7'].media)));
        } catch (err) {
            console.error(err);
            if (err) throw err;
        }
    };

    /* Loop */

    async startLoop() {

        this.eventEmitter.on('loop', async ()=> {
            let frame0 = await this.reloadRead(this.vcaps[0], this.w, this.h, this.speeds);
            let frame1 = await this.reloadRead(this.vcaps[1], this.w, this.h, this.speeds);
            let frame2 = await this.reloadRead(this.vcaps[2], this.w, this.h, this.speeds);
            let frame3 = await this.reloadRead(this.vcaps[3], this.w, this.h, this.speeds);
            let frame4 = await this.reloadRead(this.vcaps[4], this.w, this.h, this.speeds);
            let frame5 = await this.reloadRead(this.vcaps[5], this.w, this.h, this.speeds);
            let frame6 = await this.reloadRead(this.vcaps[6], this.w, this.h, this.speeds);
            let frame7 = await this.reloadRead(this.vcaps[7], this.w, this.h, this.speeds);
            let fusion0 = cv.addWeighted(frame0, this.weights[0], frame1, this.weights[1], 0);
            let fusion1 = cv.addWeighted(fusion0, 1, frame2, this.weights[2], 0);
            let fusion2 = cv.addWeighted(fusion1, 1, frame3, this.weights[3], 0);
            let fusion3 = cv.addWeighted(fusion2, 1, frame4, this.weights[4], 0);
            let fusion4 = cv.addWeighted(fusion3, 1, frame5, this.weights[5], 0);
            let fusion5 = cv.addWeighted(fusion4, 1, frame6, this.weights[6], 0);
            let fusion = cv.addWeighted(fusion5, 1, frame7, this.weights[7], 0);
    
            if (this.showDate === true) {
                const font = cv.FONT_HERSHEY_PLAIN;
                const green = new cv.Vec3(255, 255, 255);
                const date = new Date();

                fusion.putText(date.toLocaleDateString('fr', {day: 'numeric', month: 'numeric', year: 'numeric', 'hour': 'numeric', 'minute': 'numeric', 'second': 'numeric'}) + '.' + date.getMilliseconds(), new cv.Point2(260, 520), font, 7, green, 7)
            }

            if (this.canny === true && this.#tmpCanny === true) {
                fusion = fusion.canny(10, this.#speedCanny);
                this.#tmpCanny = false;
            } else if (this.canny === true) {
                this.#tmpCanny = true;
            }
    
            if (this.#fixeCanny === true) {
                fusion = fusion.canny(10, this.#speedCanny);
                this.canny = false;
            }
    
            cv.imshow('OpenVJ - Engine', fusion);

            const key = cv.waitKey(1);
            setImmediate(() => {
                this.eventEmitter.emit('loop');
            });
        });

        try {
            this.presets = await this.getPresets();
            await this.reloadPreset(() => {});
            setImmediate(() => {
                this.eventEmitter.emit('loop');
            });
        } catch (err) {
            console.error(err);
            if (err) throw err;
        }
    };

}

/* Start VJ Engine */

let vjengine = new VJEngine(process.argv[2], 1366, 768);
vjengine.startMIDI();
vjengine.startLoop();

/* Websockets and HTTP for UI/UX */

var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.use(cors());
app.use(express.static(process.argv[2]));

io.on('connection', (socket) => {
    vjengine.WSListeners.removeAllListeners('preset-change');
    socket.emit('change-preset', vjengine.currentPreset);

    vjengine.WSListeners.on('preset-change', () => {
        socket.emit('change-preset', vjengine.currentPreset);
    });
});

http.listen(8000);