/* Requires */

const { spawn } = require("child_process");
const vjengine = spawn('node', ['src/vjengine.js', 'presets']);

/* Config */

var profile = {};
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'openvj'
});

connection.connect();

/* Models */

var query = (connection, request) => {
    return new Promise((resolve, reject) => {
        connection.query(request, (err, res) => {
            if (err) throw err;
            
            resolve(res);
        });
    });
}

/* Welcome */

;(async () => {
    profile = await query(connection, 'SELECT * FROM profile');

    if (profile === []) {
        
    }
})();