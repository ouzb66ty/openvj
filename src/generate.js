const request = require('request');
const download = require('image-downloader')
const path = require('path');
const json2toml = require('json2toml');
const fs = require('fs');
let limit = process.argv[2];
let offset = process.argv[3];
let presetName = process.argv[4];
let keyword = process.argv[5];

if (process.argv.length != 6) {
  console.info('usage: node generate.js <limit> <offset> <presetName> <GIPHYKeyword>');
} else {
  request('https://api.giphy.com/v1/gifs/search?api_key=SEjduU5PNXG77kFNKSBu7NQUxeII81pY&q=' + keyword + '&limit=' + limit + '&offset=' + offset + '&rating=G&lang=en', { json: true }, async (err, res, body) => {
    if (err) { return console.log(err); }

    const presetPath = path.resolve('presets/' + presetName);

    if (fs.existsSync(presetPath)) {
      console.error('❌ Impossible car ce nom de preset existe déjà.');
      process.exit(1);
    } else {
      fs.mkdirSync(presetPath);
      console.info('Veuillez patienter un instant, génération en cours..');
    }

    var header = {
      name : presetName,
      description : 'From GIPHY, ' + keyword + ', with offset ' + offset + '.',
      author: 'Generator'
    }
    var canals = {};
  
    for (var i = 0; i < limit; i++) {

      const options = {
          url: body.data[i].images.original.url,
          dest: 'presets/' + presetName + '/' + body.data[i].title + '.gif'
      }
      
      try {

        var res = await download.image(options);
        console.log('Saved to', res.filename)
        if (i != 7)
          canals[(i + 1).toString()] = { 'media': path.basename(res.filename) };
        else
          canals['master'] = { 'media': path.basename(res.filename) };

      } catch (err) {
        console.error('❌ ' + err);
      }

    }

    var config = json2toml({ header, canals });
    console.log();
    console.info('Votre configuration TOML :');
    console.log(config);

    fs.writeFile('presets/' + presetName + '/config.toml', config , function (err) {
      if (err) console.error('❌ ' + err);

      console.info('✅ Génération du preset terminé.');
    });

  });
}